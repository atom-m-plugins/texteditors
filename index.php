<?php

class bbcodes {
    public function common($params) {

        $markers = array(
            'editor_head' => null,
            'editor_body' => null,
            'editor_buttons' => null,
            'editor_text' => null,
            'editor_forum_text' => null,
            'editor_forum_quote' => null,
            'editor_forum_name' => null,
        );

        $editor_set = array();
        $plugin_path = get_url('/plugins/bbcodes');

        include 'config.php';

        if ($editor_set && is_array($editor_set) && count($editor_set)) {
            $number = 0;
            foreach ($editor_set as $index => $editor) {
                if (isset($editor['default']) && $editor['default']) {
                    $number = $index;
                    break;
                }
            }
            $editor = $editor_set[$number];

            $markers = array(
                'editor_head' => null,
                'editor_body' => null,
                'editor_buttons' => null,
                'editor_text' => null,
                'editor_forum_text' => null,
                'editor_forum_quote' => null,
                'editor_forum_name' => null,
            );
            foreach ($markers as $marker => $value) {
                $markers[$marker] = !empty($editor[$marker]) ? $editor[$marker] : null;

                $data = $editor[$marker];
                $data = str_replace("%plugin_path%", $plugin_path, $data);

                $params[$marker] = $data;
            }
            
            $params['editor_forum_quote'] = function($arg) use($editor, $plugin_path) {
                if (count($arg)>0) {
                    $arg = $arg[0];
                    $data = $editor['editor_forum_quote'];
                    $data = str_replace("%plugin_path%", $plugin_path, $data);
                    $data = str_replace("%author_name%", $arg, $data);
                    return $data;
                } else {
                    return;
                }
            };
            
            $params['editor_forum_name'] = function($arg) use($editor, $plugin_path) {
                if (count($arg)>0) {
                    $arg = $arg[0];
                    $data = $editor['editor_forum_name'];
                    $data = str_replace("%plugin_path%", $plugin_path, $data);
                    $data = str_replace("%author_name%", $arg, $data);
                    return $data;
                } else {
                    return;
                }
            };
            
        }

        return $params;
    }
}
